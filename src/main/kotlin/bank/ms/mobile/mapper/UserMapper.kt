package bank.ms.mobile.mapper

import bank.ms.mobile.dao.UserEntity
import bank.ms.mobile.model.UserDto

fun UserEntity.toDto(): UserDto = UserDto(
    uuid = uuid,
    username = username,
    email = email,
    status = status
)