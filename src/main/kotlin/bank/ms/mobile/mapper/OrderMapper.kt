package bank.ms.mobile.mapper

import bank.ms.mobile.dao.OrderEntity
import bank.ms.mobile.dao.UserEntity
import bank.ms.mobile.model.OrderDto
import bank.ms.mobile.model.OrderStatus
import bank.ms.mobile.model.UserDto

fun OrderDto.toEntity(userEntity: UserEntity): OrderEntity = OrderEntity(
    userEntity = userEntity,
    stockSymbol = stockSymbol,
    count = count,
    orderType = orderType,
    status = OrderStatus.ACTIVE,
    targetPrice = targetPrice
)

