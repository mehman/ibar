package bank.ms.mobile.dao

import bank.ms.mobile.model.UserStatus
import org.hibernate.annotations.CreationTimestamp
import org.hibernate.annotations.UpdateTimestamp
import java.time.LocalDateTime
import java.util.*
import javax.persistence.*

@Entity
@Table(name = "users")
data class UserEntity(
    @Id
    @Column(name = "id")
    var uuid: String,

    @Column(name = "username", unique = true)
    var username: String,


    @Column(name = "email", unique = true)
    var email: String,
    var password: String,

    @Enumerated(EnumType.STRING)
    var status: UserStatus,

    @CreationTimestamp
    @Column(name = "created_at")
    var createdAt: LocalDateTime? = null,

    @UpdateTimestamp
    @Column(name = "updated_at")
    var updatedAt: LocalDateTime? = null
)