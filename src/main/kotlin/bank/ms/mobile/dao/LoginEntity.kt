package bank.ms.mobile.dao

import org.hibernate.annotations.CreationTimestamp
import org.hibernate.annotations.UpdateTimestamp
import java.time.LocalDateTime
import javax.persistence.*

@Entity
@Table(name = "user_logins")
data class LoginEntity(
    @Id
    @Column(name = "token")
    var token: String,

    @ManyToOne
    @JoinColumn(name = "user_id")
    var userEntity: UserEntity,

    @Column(name = "expire_date_time")
    var expireDateTime: LocalDateTime? = null,

    @CreationTimestamp
    @Column(name = "created_at")
    var createdAt: LocalDateTime? = null,

    @UpdateTimestamp
    @Column(name = "updated_at")
    var updatedAt: LocalDateTime? = null
)