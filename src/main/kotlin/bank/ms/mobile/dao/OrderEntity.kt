package bank.ms.mobile.dao

import bank.ms.mobile.model.OrderStatus
import bank.ms.mobile.model.OrderType
import org.hibernate.annotations.CreationTimestamp
import org.hibernate.annotations.UpdateTimestamp
import java.math.BigDecimal
import java.time.LocalDateTime
import javax.persistence.*

@Entity
@Table(name = "user_orders")
data class OrderEntity(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    var id: Long? = null,

    @ManyToOne
    @JoinColumn(name = "user_id")
    var userEntity: UserEntity,

    @Column(name = "stock_symbol")
    var stockSymbol: String,

    @Column(name = "count")
    var count: Int,

    @Enumerated(EnumType.STRING)
    @Column(name = "order_type")
    val orderType: OrderType,

    @Column(name = "target_price")
    val targetPrice: BigDecimal,

    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    var status: OrderStatus,

    @CreationTimestamp
    @Column(name = "created_at")
    var createdAt: LocalDateTime? = null,

    @UpdateTimestamp
    @Column(name = "updated_at")
    var updatedAt: LocalDateTime? = null
)