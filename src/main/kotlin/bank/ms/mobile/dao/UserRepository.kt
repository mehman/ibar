package bank.ms.mobile.dao

import bank.ms.mobile.model.UserStatus
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface UserRepository : JpaRepository<UserEntity, String> {
    fun findByUsername(username: String): UserEntity
    fun findByUuidAndStatus(id: String, status: UserStatus): UserEntity
}