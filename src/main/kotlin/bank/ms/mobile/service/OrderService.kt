package bank.ms.mobile.service

import bank.ms.mobile.client.mail.EmailDto
import bank.ms.mobile.client.mail.MailServerClient
import bank.ms.mobile.dao.LoginRepository
import bank.ms.mobile.dao.OrderEntity
import bank.ms.mobile.dao.OrderRepository
import bank.ms.mobile.mapper.toEntity
import bank.ms.mobile.model.OrderDto
import bank.ms.mobile.model.OrderStatus
import bank.ms.mobile.model.OrderType
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import java.util.function.Consumer

@Service
class OrderService(
    private val orderRepository: OrderRepository,
    private val loginRepository: LoginRepository,
    private val stockService: StockService,
    private val mailServerClient: MailServerClient
) {
    companion object {
        private val logger = LoggerFactory.getLogger(this::class.java)
    }

    fun createOrder(token: String, orderDto: OrderDto): Boolean {
        logger.info("OrderService.createOrder.start. Request: {}, {}", token, orderDto)
        val loginEntity = loginRepository.findById(token).get()
        val orderEntity = orderDto.toEntity(loginEntity.userEntity)
        orderRepository.save(orderEntity)
        logger.info("OrderService.createOrder.end")
        return true
    }

    fun processOrder() {
        val orderList = orderRepository.findByStatus(OrderStatus.ACTIVE)
        val stockList = stockService.getStockList()

        orderList.forEach(Consumer<OrderEntity> { orderEntity: OrderEntity ->
            run {
                val stock = stockList.filter { it.symbol == orderEntity.stockSymbol }

                if (orderEntity.orderType.equals(OrderType.BUY) && stock.get(0).price <= orderEntity.targetPrice) {
                    makePayment(orderEntity)
                }

                if (orderEntity.orderType.equals(OrderType.SELL) && stock.get(0).price >= orderEntity.targetPrice) {
                    makePayment(orderEntity)
                }
            }
        })
    }

    fun makePayment(orderEntity: OrderEntity) {
        logger.info("OrderService.makePayment.start. order: {}", orderEntity)
        orderEntity.status = OrderStatus.COMPLETED
        orderRepository.save(orderEntity)

        mailServerClient.sendEmail(
            EmailDto(
                orderEntity.userEntity.email,
                "make Payment",
                "Your order was completed " + orderEntity
            )
        )

        logger.info("OrderService.makePayment.end.")
    }
}