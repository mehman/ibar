package bank.ms.mobile.service

import bank.ms.mobile.client.stock.ExchangeServerClient
import bank.ms.mobile.client.stock.StockDto
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service

@Service
class StockService(
    private val exchangeServerClient: ExchangeServerClient
) {

    companion object {
        private val logger = LoggerFactory.getLogger(this::class.java)
    }

    fun getStockList(): List<StockDto> {
        logger.info("StockService.getStockList.start")
        val stockDtoList = exchangeServerClient.getStockList()
        logger.info("StockService.getStockList.end")
        return stockDtoList
    }
}