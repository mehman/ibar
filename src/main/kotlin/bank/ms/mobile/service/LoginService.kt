package bank.ms.mobile.service

import bank.ms.mobile.client.mail.EmailDto
import bank.ms.mobile.client.mail.MailServerClient
import bank.ms.mobile.dao.LoginEntity
import bank.ms.mobile.dao.LoginRepository
import bank.ms.mobile.dao.UserEntity
import bank.ms.mobile.dao.UserRepository
import bank.ms.mobile.mapper.toDto
import bank.ms.mobile.model.*
import org.springframework.stereotype.Service
import java.time.LocalDateTime
import java.util.*

@Service(value = "loginService")
class LoginService(
    private val userRepository: UserRepository,
    private val loginRepository: LoginRepository,
    private val mailServerClient: MailServerClient
) {

    fun registerUser(dto: RegisterRequestDto): UserDto {

        val userEntity = UserEntity(
            uuid = UUID.randomUUID().toString(),
            username = dto.username,
            email = dto.email,
            password = dto.password,
            status = UserStatus.NEW
        )

        val result = mailServerClient.sendEmail(EmailDto(dto.email, "Activation", userEntity.uuid))

        if (result) {
            userRepository.save(userEntity)
            return userEntity.toDto()
        } else {
            throw Exception("Email was not sent. Please, write correct email.")
        }
    }

    fun confirmUser(dto: ConfirmRequestDto): UserDto {

        val userEntity = userRepository.findByUsername(dto.username)

        if (userEntity.uuid.equals(dto.uuid)) {
            userEntity.status = UserStatus.ACTIVE
            userRepository.save(userEntity)
        } else {
            throw Exception("UUID is not correct")
        }

        return userEntity.toDto()
    }

    fun login(dto: LoginRequestDto): LoginResponseDto {

        val userEntity = userRepository.findByUuidAndStatus(dto.uuid, UserStatus.ACTIVE)
        var loginEntity: LoginEntity?

        if (userEntity.password.equals(dto.password)) {
            loginEntity = LoginEntity(
                token = UUID.randomUUID().toString(),
                userEntity = userEntity,
                expireDateTime = LocalDateTime.now().plusMinutes(30)
            )

            loginRepository.save(loginEntity)
        } else {
            throw Exception("Password is not correct")
        }

        return LoginResponseDto(loginEntity!!.token)
    }

    fun refleshToken(token: String) {
        val loginEntity = loginRepository.findById(token).get()
        loginEntity.expireDateTime = LocalDateTime.now().plusMinutes(30)
        loginRepository.save(loginEntity)
    }

    fun logout(token: String) {
        val loginEntity = loginRepository.findById(token).get()
        loginEntity.expireDateTime = LocalDateTime.now()
        loginRepository.save(loginEntity)
    }

    fun validateToken(token: String): Boolean {
        val loginEntity = loginRepository.findById(token).get()
        return loginEntity.expireDateTime!!.isAfter(LocalDateTime.now())
    }
}