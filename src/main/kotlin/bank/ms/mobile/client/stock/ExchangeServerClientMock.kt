package bank.ms.mobile.client.stock

import org.springframework.stereotype.Component
import java.math.BigDecimal

@Component
class ExchangeServerClientMock : ExchangeServerClient {
    override fun getStockList(): List<StockDto> {
        return listOf(
            StockDto("FB", "Facebook", BigDecimal(12.56)),
            StockDto("AMZN", "Amazon", BigDecimal(6.53)),
            StockDto("GOGL", "Google", BigDecimal(15.72))
        )
    }
}