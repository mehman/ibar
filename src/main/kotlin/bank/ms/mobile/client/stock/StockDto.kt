package bank.ms.mobile.client.stock

import java.math.BigDecimal

data class StockDto(
    val symbol: String,
    val title: String,
    val price: BigDecimal
)