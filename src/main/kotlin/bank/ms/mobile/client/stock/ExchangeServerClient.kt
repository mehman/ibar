package bank.ms.mobile.client.stock

import org.springframework.cloud.openfeign.FeignClient
import org.springframework.context.annotation.Profile
import org.springframework.web.bind.annotation.PostMapping

@Profile("prod")
@FeignClient(name = "ms-exhange-server", url = "\${client.exchange.url}")
interface ExchangeServerClient {
    @PostMapping("/stocks")
    fun getStockList(): List<StockDto>
}