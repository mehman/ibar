package bank.ms.mobile.client.mail

import org.springframework.cloud.openfeign.FeignClient
import org.springframework.context.annotation.Profile
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody

@Profile("prod")
@FeignClient(name = "ms-mail-server", url = "\${client.mail.url}")
interface MailServerClient {
    @PostMapping("/sendEmail")
    fun sendEmail(@RequestBody dto: EmailDto): Boolean
}