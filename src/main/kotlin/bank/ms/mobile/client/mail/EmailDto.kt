package bank.ms.mobile.client.mail

data class EmailDto(
    val email: String,
    val title: String,
    val subject: String
)