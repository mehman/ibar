package bank.ms.mobile.client.mail

import org.springframework.stereotype.Component

@Component
class MailServerClientMock : MailServerClient {
    override fun sendEmail(dto: EmailDto): Boolean {
        return true;
    }
}