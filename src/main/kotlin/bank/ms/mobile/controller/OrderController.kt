package bank.ms.mobile.controller

import bank.ms.mobile.model.OrderDto
import bank.ms.mobile.service.LoginService
import bank.ms.mobile.service.OrderService
import io.swagger.annotations.Api
import org.springframework.web.bind.annotation.*

@Api(value = "Order endpoints")
@RestController
@RequestMapping("v1/mobile/orders")
class OrderController(
    private val loginService: LoginService,
    private val orderService: OrderService
) {

    @PostMapping("")
    fun createOrder(
        @RequestHeader("token") token: String,
        @RequestBody orderDto: OrderDto
    ): Boolean {
        if (!loginService.validateToken(token)) {
            throw Exception("Token is not correct!")
        }

        return orderService.createOrder(token, orderDto)
    }
}