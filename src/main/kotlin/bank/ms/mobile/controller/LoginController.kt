package bank.ms.mobile.controller

import bank.ms.mobile.model.*
import bank.ms.mobile.service.LoginService
import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@Api(value = "Public registration endpoints")
@RestController
@RequestMapping("v1/mobile/user")
class LoginController(private val loginService: LoginService) {

    @ApiOperation("Starts registration flow (Step 1)")
    @PostMapping("/register")
    fun registerUser(@RequestBody registerRequestDto: RegisterRequestDto): UserDto =
        loginService.registerUser(registerRequestDto)

    @PostMapping("/confirm")
    fun confirmUser(@RequestBody confirmRequestDto: ConfirmRequestDto): UserDto =
        loginService.confirmUser(confirmRequestDto)

    @PostMapping("/login")
    fun login(@RequestBody loginRequestDto: LoginRequestDto): LoginResponseDto =
        loginService.login(loginRequestDto)

    @PostMapping("/reflesh_token")
    fun refleshToken(@RequestBody token: String) =
        loginService.refleshToken(token)

    @PostMapping("/logout")
    fun logout(@RequestBody token: String) =
        loginService.logout(token)

    @PostMapping("/validate_token")
    fun validateToken(@RequestBody token: String): Boolean =
        loginService.validateToken(token)
}