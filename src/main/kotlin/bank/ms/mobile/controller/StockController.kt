package bank.ms.mobile.controller

import bank.ms.mobile.client.stock.StockDto
import bank.ms.mobile.service.LoginService
import bank.ms.mobile.service.StockService
import io.swagger.annotations.Api
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestHeader
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@Api(value = "Stocks endpoints")
@RestController
@RequestMapping("v1/mobile/stocks")
class StockController(private val loginService: LoginService,
                      private val stockService: StockService) {

    @GetMapping("")
    fun getStockList(@RequestHeader("token") token: String): List<StockDto> {
        if(!loginService.validateToken(token)) {
            throw Exception("Token is not correct!")
        }

        return stockService.getStockList()
    }
}