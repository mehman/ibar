package bank.ms.mobile.model

enum class UserStatus {
    NEW, ACTIVE, DELETED
}