package bank.ms.mobile.model

enum class OrderStatus {
    ACTIVE, DELETED, COMPLETED
}