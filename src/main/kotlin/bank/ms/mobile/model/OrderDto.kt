package bank.ms.mobile.model

import java.math.BigDecimal

data class OrderDto(
    var stockSymbol: String,
    var count: Int,
    val orderType: OrderType,
    val targetPrice: BigDecimal
)