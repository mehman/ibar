package bank.ms.mobile.model

import io.swagger.annotations.ApiModel

@ApiModel("Registration request dto")
data class RegisterRequestDto(
    val username: String,
    val email: String,
    val password: String
)