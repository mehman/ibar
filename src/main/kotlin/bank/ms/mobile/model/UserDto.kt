package bank.ms.mobile.model

import java.util.*

data class UserDto(
    val uuid: String = UUID.randomUUID().toString(),
    val username: String,
    val email: String,
    val status: UserStatus
)