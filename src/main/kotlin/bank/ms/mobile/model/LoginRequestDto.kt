package bank.ms.mobile.model

import io.swagger.annotations.ApiModel

@ApiModel("Login request dto")
data class LoginRequestDto(
    val uuid: String,
    val password: String
)