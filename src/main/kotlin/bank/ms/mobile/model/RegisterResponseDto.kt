package bank.ms.mobile.model

import io.swagger.annotations.ApiModel
import java.util.*

@ApiModel("Registration response dto")
data class RegisterResponseDto(
    val uuid: String = UUID.randomUUID().toString(),
    val userDto: UserDto
)