package bank.ms.mobile.model

enum class OrderType {
    BUY, SELL
}