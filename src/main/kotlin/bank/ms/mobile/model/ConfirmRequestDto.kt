package bank.ms.mobile.model

import io.swagger.annotations.ApiModel

@ApiModel("Registration request dto")
data class ConfirmRequestDto(
    val uuid: String,
    val username: String
)