package bank.ms.mobile.model

import io.swagger.annotations.ApiModel

@ApiModel("Login response dto")
data class LoginResponseDto(
    val token: String
)