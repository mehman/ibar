package bank.ms.mobile.scheduler

import bank.ms.mobile.service.OrderService
import org.slf4j.LoggerFactory
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Component

@Component
class OrderScheduler(private val orderService: OrderService) {

    companion object {
        private val logger = LoggerFactory.getLogger(this::class.java)
    }

    @Scheduled(fixedDelayString = "\${scheduler.process_orders.delay}")
    fun processOrder() {
        logger.info("OrderScheduler.processOrder.start")
        orderService.processOrder()
        logger.info("OrderScheduler.processOrder.end")
    }
}